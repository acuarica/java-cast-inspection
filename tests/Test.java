
public class Test {

	public void invoke(Object o) {
	}

    public void testNull() {
        invoke((String)null);
        invoke((String)"null");
        invoke((Integer)5);
        invoke((long)5);
    }

}
