

cd cache/
for f in *; do
  echo "File -> $f"
  cd $f
  ./gradlew build >> ../../out/stdout-err/$f-gradlew.out 2>&1
  cd ..
done
