#!/usr/bin/python

query = "https://api.github.com/search/repositories?q=stars:%3E=10000+language:java&sort=stars&order=desc&per_page=100&page={}"

import urllib2, json, time, sys

from pprint import pprint

page=1
count=None

while True:
    sys.stderr.write("Using page no. {}\n".format(page))
    pquery = query.format(page)
    sys.stderr.write("Fetching {}...\n".format(pquery))

    if count is not None:
        secs = 60
        sys.stderr.write("Waiting {}s before doing next request ...\n".format(secs))
        time.sleep(secs)

    response = urllib2.urlopen(pquery)
    resp = response.read()

    repos = json.loads(resp)
    if count is None:
        count = repos['total_count']
        sys.stderr.write("Total repos count: {}\n".format(count))

    for repo in repos['items']:
        print repo['clone_url']

    r = len(repos['items'])
    sys.stderr.write("Items fetched: {}\n".format(r))
    if count > r:
        count -= r
        page += 1
    else:
        break

