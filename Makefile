
#export ANDROID_HOME=/Users/luigi/Library/Android/sdk

CASTS=casts-`date +'%Y.%m.%d_%H.%M.%S'`.out

casts:
	@find repos -name "*.java" -exec ./findcast.py {} \; > $(CASTS)

test:
	./findcast.py tests/Test.java

fetchrepos: | repos/
	./fetchrepos.sh

repos/:
	mkdir -p $@

cleanrepos:
	rm -rf repos/
