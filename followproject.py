#!/usr/bin/python

import requests, time

nonce = "4c4e84ae8d0969db156dc6a4d613728ed49335555e9d64d8dbde531ecdd7d674853b93bb929384f1842e9c0936fdf573fd8a85137b221dbdc8179ff418cb030b"
apiVersion = "3a670395fe843b778219bcb2c371639e2407496a"
cookies = {
  'JSESSIONID': 'Fm4qUHKEiK7rulx3RYBI8g',
  'lgtm_long_session': '9c343cca24524744b8423cc1daf31ad50f960609195061a3c6a43a40698ac2d4b6fd8580c534fd5a42401f8f2f68312b8dac232dbed675eb633913207c80d34d'
  }

def follow(url):
	r = requests.post("https://lgtm.com/internal_api/v0.2/followProject", data={'url': url, 'nonce': nonce, 'apiVersion': apiVersion}, cookies=cookies)
	print r.status_code, r.reason
	print(r.text)

pause = False
with open("repos1000-work.list") as f:
	for n, url in enumerate(f):
		if pause:
			time.sleep(60)
		else:
			pause = True

		url = url.rstrip('\n')
		print "[{}] Following project {} ...".format(n+1, url),
		follow(url)

#curl 'https://lgtm.com/internal_api/v0.2/followProject' \ -XPOST \ -H 'Referer: https://lgtm.com/dashboard' \ -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \ -H 'Origin: https://lgtm.com' \ -H 'Host: lgtm.com' \ -H 'Accept: */*' \ -H 'Connection: keep-alive' \ -H 'Accept-Language: en-us' \ -H 'Accept-Encoding: gzip, deflate' \ -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/604.3.5 (KHTML, like Gecko) Version/11.0.1 Safari/604.3.5' \ -H 'Cookie: _ga=GA1.2.1834729504.1515682899; _gat=1; _gid=GA1.2.324141036.1515682899; lgtm_long_session=6d9a827a658cdd33045d59965a824078c7fee8c57e08a6397762f40ed3871d10421e8f692241602ee9c9fccea83bef790b643ef2e7653ae38625ef09a2071dcc; JSESSIONID=R_2dVHvXonH3wbqyZe1Grw' \ -H 'Content-Length: 199' \ -H 'X-Requested-With: XMLHttpRequest' \ --data 'url=asdfasdf&nonce=967425bd4f6bb0656cfe0d415f0fd987744ef0c552673e4085e0b805de24ca83b7632ea16ce9617c4905bef45210c25868e1efa0dc2ac1faabb3d3d1803b91e5&apiVersion=3a670395fe843b778219bcb2c371639e2407496a'
