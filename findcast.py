#!/usr/bin/python

import pprint

import javalang
import sys

filename = sys.argv[1]

with open(filename, "r") as f:
  src = f.read()

i = 1

null = 0

tree = javalang.parse.parse(src)

def context(node, path):
    pos = ""
    for parent in path:
        if isinstance(parent, javalang.tree.Node) and hasattr(parent, '_position'):
            pos = "%s:%d" % (filename, parent._position[0])
        if isinstance(parent, javalang.tree.MethodDeclaration):
            print "in method", parent.name,
        if isinstance(parent, javalang.tree.ConstructorDeclaration):
            print "in constructor", parent.name,
        if isinstance(parent, javalang.tree.EnumDeclaration):
            print "in enum", parent.name,
        if isinstance(parent, javalang.tree.VariableDeclarator):
            print "in variable declaration", parent.name,
        if isinstance(parent, javalang.tree.InterfaceDeclaration):
            print "in interface", parent.name,
        if isinstance(parent, javalang.tree.ClassDeclaration):
            print "in class", parent.name,

    if pos != "":
      print
      print pos

for path, node in tree.filter(javalang.tree.Cast):
    print i, "cast", '(' + node.type.name + ')', node.expression,
    context(node, list(path))
    i += 1

    if isinstance(node.type, javalang.tree.ReferenceType):
      if isinstance(node.expression, javalang.tree.Literal) and node.expression.value == 'null':
        print 'pattern: null'
      elif isinstance(node.expression, javalang.tree.MethodInvocation):
        pprint.pprint(node.expression.__dict__)
        for m in node.expression.arguments:
          pprint.pprint(m.__dict__)
        print 'pattern: <???>'
      else:
        print 'pattern: <???>'
    else:
      assert isinstance(node.type, javalang.tree.BasicType)
      print 'pattern: conversion'

for path, node in tree.filter(javalang.tree.BinaryOperation):
    if node.operator == "instanceof":
        print i, "instanceof", node.operandl, node.operandr.name
        context(node, list(path))
        i += 1
