
export ANDROID_HOME=/Users/luigi/Library/Android/sdk

cd cache/
for f in *; do
  echo "File -> $f"
  cd $f
  ./mvnw compile >> ../../out/stdout-err/$f-mavenw.out 2>&1
  cd ..
done
