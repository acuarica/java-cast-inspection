package ch.usi.inf.sape.caststudy;

import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.util.JavacTask;
import com.sun.source.util.TreePath;

import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;
import java.io.PrintWriter;
import java.util.function.Consumer;

class UrlOpenConnectionCastVisitor extends CastVisitor {

    UrlOpenConnectionCastVisitor(JavacTask task, PrintWriter out) {
        super(task, out);
    }

    @Override
    public Void visitTypeCast(TypeCastTree node, Void p) {
        whenMethod(node.getExpression(), t -> {
            if (t._1 == elements.getName("openConnection")) {
                out.println("openconnection");
            }
        });
        return super.visitTypeCast(node, p);
    }

    private static class Tuple<S, T> {
        S _1;
        T _2;

        public Tuple(S name, T type) {
            _1 = name;
            _2 = type;
        }
    }

    private void whenMethod(ExpressionTree tree, Consumer<Tuple<Name, TypeMirror>> f) {
        final TreePath path = new TreePath(getCurrentPath(), tree);

        switch (path.getLeaf().getKind()) {
            // is it a Method Invocation?
            case METHOD_INVOCATION:
                MethodInvocationTree methodInvocation = (MethodInvocationTree) path.getLeaf();
                // extract the identifier and receiver (methodSelectTree)
                ExpressionTree methodSelect = methodInvocation.getMethodSelect();
                switch (methodSelect.getKind()) {
                    case MEMBER_SELECT:
                        // extract receiver
                        MemberSelectTree mst = (MemberSelectTree) methodSelect;
                        ExpressionTree expr = mst.getExpression();
                        // get type of extracted receiver
                        TypeMirror type = trees.getTypeMirror(new TreePath(path, expr));
                        // extract method name
                        Name name = mst.getIdentifier();
                        // 1) check if receiver's type is subtype of java.util.Map
                        // 2) check if the extracted method name is exactly "get"
                        f.accept(new Tuple<>(name, type));
                }
        }
    }

}
