package ch.usi.inf.sape.caststudy;

import com.sun.source.tree.Tree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.util.JavacTask;

import java.io.PrintWriter;

class NullCastVisitor extends CastVisitor {

    NullCastVisitor(JavacTask task, PrintWriter out) {
        super(task, out);
    }

    @Override
    public Void visitTypeCast(TypeCastTree node, Void p) {
        if (node.getExpression().getKind() == Tree.Kind.NULL_LITERAL) {
            out.println(node+" ");
        }

        return super.visitTypeCast(node, p);
    }
}
