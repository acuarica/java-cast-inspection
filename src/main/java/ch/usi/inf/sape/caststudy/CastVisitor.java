package ch.usi.inf.sape.caststudy;

import com.sun.source.tree.*;
import com.sun.source.tree.Tree.Kind;
import com.sun.source.util.*;

import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import java.io.PrintWriter;

class CastVisitor extends TreePathScanner<Void, Void> {

    private final SourcePositions sourcePositions;
    protected final Trees trees;
    protected final Types types;
    protected final Elements elements;
    private final TypeMirror mapType;
    private final Name getName;
    protected final PrintWriter out;
    private CompilationUnitTree currCompUnit;

    class CastListener implements TaskListener {

        @Override
        public void finished(TaskEvent taskEvent) {
            if (taskEvent.getKind().equals(TaskEvent.Kind.ANALYZE)) {
                CompilationUnitTree compilationUnit = taskEvent.getCompilationUnit();
                CastVisitor.this.scan(compilationUnit, null);
                CastVisitor.this.out.flush();
            }
        }

        @Override
        public void started(TaskEvent taskEvent) {
        }
    }

    CastVisitor(JavacTask task, PrintWriter out) {
        types = task.getTypes();
        trees = Trees.instance(task);
        sourcePositions = trees.getSourcePositions();
        elements = task.getElements();
        mapType = elements.getTypeElement("java.util.Map").asType();
        getName = elements.getName("get");
        this.out = out;
    }

    @Override
    public Void visitCompilationUnit(CompilationUnitTree tree, Void p) {
//        System.out.println(tree.getSourceFile().getName());
        currCompUnit = tree;
        return super.visitCompilationUnit(tree, p);
    }

    @Override
    public Void visitTypeCast(TypeCastTree node, Void p) {
//        TypeMirror type = trees.getTypeMirror(new TreePath(getCurrentPath(), node.getExpression()));

//        System.out.print(type);
//        System.out.println(node + ":" + node.getType() + ":" + node.getExpression() + "/" + node.getExpression().getKind());
//        out.println(node + ":" + node.getType() + ":" + node.getExpression() + "/" + node.getExpression().getKind());
        return super.visitTypeCast(node, p);
    }

    //    @Override
    public Void visitBinary32(BinaryTree tree, Void p) {
        // unpack left and right hand side
        ExpressionTree left = tree.getLeftOperand();
        ExpressionTree right = tree.getRightOperand();
        Kind kind = tree.getKind();

        // apply our code pattern logic
        if (isGetCallOnMap(new TreePath(getCurrentPath(), left))
                && kind == Kind.EQUAL_TO
                && isNullLiteral(right)) {
            System.out.println("Found Match at line: "
                    + getLineNumber(tree) + " in "
                    + currCompUnit.getSourceFile().getName());
        }
        return super.visitBinary(tree, p);
    }

    private boolean isNullLiteral(ExpressionTree node) {
        // is this expression representing "null"?
        return (node.getKind() == Kind.NULL_LITERAL);
    }

    private boolean isGetCallOnMap(TreePath path) {
        switch (path.getLeaf().getKind()) {
            // is it a Method Invocation?
            case METHOD_INVOCATION:
                MethodInvocationTree methodInvocation = (MethodInvocationTree) path.getLeaf();
                // extract the identifier and receiver (methodSelectTree)
                ExpressionTree methodSelect = methodInvocation.getMethodSelect();
                switch (methodSelect.getKind()) {
                    case MEMBER_SELECT:
                        // extract receiver
                        MemberSelectTree mst = (MemberSelectTree) methodSelect;
                        ExpressionTree expr = mst.getExpression();
                        // get type of extracted receiver
                        TypeMirror type = trees.getTypeMirror(new TreePath(path, expr));
                        // extract method name
                        Name name = mst.getIdentifier();
                        // 1) check if receiver's type is subtype of java.util.Map
                        // 2) check if the extracted method name is exactly "get"
                        if (types.isAssignable(types.erasure(type), types.erasure(mapType))
                                && name == getName) {
                            return true;
                        }
                }
        }
        return false;
    }

    private long getLineNumber(Tree tree) {
        // map offsets to line numbers in source file
        LineMap lineMap = currCompUnit.getLineMap();
        if (lineMap == null)
            return -1;
        // find offset of the specified AST node
        long position = sourcePositions.getStartPosition(currCompUnit, tree);
        return lineMap.getLineNumber(position);
    }

}
