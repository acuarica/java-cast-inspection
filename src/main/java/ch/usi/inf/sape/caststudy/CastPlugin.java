
package ch.usi.inf.sape.caststudy;

import com.sun.source.util.JavacTask;
import com.sun.source.util.Plugin;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class CastPlugin implements Plugin {

    @Override
    public String getName() {
        System.out.println("Running direct!");
        return "ch.usi.inf.sape.caststudy.CastPlugin";
    }

    private static PrintWriter getOut(String name) throws FileNotFoundException {
        return new PrintWriter("/Users/luigi/repos/java-cast-inspection/out/" + name + ".txt");
    }

    @Override
    public void init(JavacTask task, String... args) {
        System.out.println("init cp:" + task);

        try {
            task.addTaskListener(new CastVisitor(task, getOut("casts")).new CastListener());
            task.addTaskListener(new NullCastVisitor(task, getOut("null")).new CastListener());
//            task.addTaskListener(new UrlOpenConnectionCastVisitor(task, getOut("url")).new CastListener());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
